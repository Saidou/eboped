
### Import data -----------------------------------------------------------


data_demogrophic <- 
  read_excel("data/Demographic_final_data.xlsx") %>%
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_")))


EXCEL_solicited <-
  read_excel("data/EXCEL_local_and_solicited_2023-03-29.xlsx") %>% 
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_"))) %>%
  filter(!is.na(vaccine))

EXCEL_Unsolicited <- 
  read_excel("data/EXCEL_Unsolicited_local_and_systemic__2023-04-03.xlsx", skip = 30) %>% 
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_"))) %>% 
  select(participant_id, vaccine = impdose_e2_c4, uae_s_desc_e3_c9_1, uae_s_desc_e3_c9_2,
         uae_s_desc_e4_c12_1, uae_s_desc_e5_c17_1, uae_s_desc_e5_c17_2, uae_s_desc_e5_c17_3,
         uae_s_desc_e6_c22_1, uae_s_desc_e6_c22_2, uae_s_desc_e6_c22_3, uae_s_desc_e7_c27_1,
         uae_s_desc_e7_c27_2, uae_l_desc_e8_c30_1, uae_l_desc_e8_c30_2, uae_s_desc_e8_c30_1,
         uae_s_desc_e8_c30_2) %>% 
  mutate(vaccine = case_when(vaccine == 1 ~ "Ervebo", vaccine == 2 ~ "Varilrix"))

EXCEL_AES <- 
  read_excel("data/EXCEL_AES__2023-03-23-122920561.xlsx", skip = 28) %>%
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_"))) %>% 
  filter(!is.na(impdose_e2_c4)) %>% 
  mutate(vaccine = case_when(impdose_e2_c4 == 1 ~ "Ervebo", impdose_e2_c4 == 2 ~ "Varilrix")) %>% 
  select(participant_id, vaccine, starts_with("ae_desc_"))

EXCEL_Biochemistry <-
  read_excel("data/EXCEL_Biochemistry_Results_2023-03-20-103827415.xlsx", skip = 30) %>%
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_"))) %>% 
  filter(!is.na(impdose_e2_c4)) %>% 
  mutate(vaccine = case_when(impdose_e2_c4 == 1 ~ "Ervebo", impdose_e2_c4 == 2 ~ "Varilrix")) %>% 
  select(-c(protocol_id : age_e1_c1, contains("version_id"), impdose_e2_c4))

EXCEL_hematology <- 
  read_excel("data/EXCEL_Final_Results_hematology_12_2023-03-21-113622500.xlsx", skip = 30) %>%
  rename_all(list(~str_replace_all(tolower(.), "[ ]", "_"))) %>% 
  filter(!is.na(impdose_e2_c4)) %>%  
  mutate(vaccine = case_when(impdose_e2_c4 == 1 ~ "Ervebo", impdose_e2_c4 == 2 ~ "Varilrix")) %>% 
  select(-c(protocol_id : age_e1_c1, contains("version_id"), impdose_e2_c4))

